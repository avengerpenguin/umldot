#!/bin/sh

FILEBASE="`basename \"$1\" .dot`"
OUTFILE="${FILEBASE}.png"

SCRIPTDIR="$( cd "$( dirname "$0" )" && pwd )"

dot -l"${SCRIPTDIR}/sdl.ps" -Tps "$1" | convert -density 150 -geometry 100% -flatten - "$OUTFILE"
